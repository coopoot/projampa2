package com.projampa.dsouza.projampa3.View;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.images.Size;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.projampa.dsouza.projampa3.Model.Config;
import com.projampa.dsouza.projampa3.Model.RequestHandler;
import com.projampa.dsouza.projampa3.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class OuvidoriaActivity extends Activity {

    EditText edNome;
    EditText edEmail;
    EditText edEndereco;
    EditText edNumero;
    EditText edBairro;
    EditText edMensagem;
    Button btMenu;
    Button btEnviar;
    Button btAnexo;
    Intent envio;
    ImageView imageView;
    TextView tvAnexo;
    String localImagem = "";
    private String urlImagem;
    private static final int IMAGE_REQUEST = 1;
    private static final int MY_REQUEST_CODE = 123;
    ProgressDialog mProgress;
    Uri filePath;
    AlertDialog alertaDialog;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    //StorageReference storageRef = storage.getReferenceFromUrl("gs://projampa2.appspot.com");
    StorageReference storageRef = storage.getReferenceFromUrl("gs://projampa-107cb.appspot.com");
    String data;
    SimpleDateFormat formatoDataAtual;
    private Uri uri = null;
    private String mCurrentPhotoPath;
    private static final String TAG = "MainActivity";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ouvidoria);
        mProgress = new ProgressDialog(this);
        String dataN = DateFormat.getDateInstance().format(new Date());

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected()) {
        }else{
            Toast.makeText(getBaseContext(), "Sua conexão com a internet falhou!!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,PrincipalActivity.class);
            startActivity(intent);
        }

        if( Locale.getDefault().getCountry().equals("BR")) {
            formatoDataAtual = new SimpleDateFormat("dd/MM/yyyy");
        }else{
            formatoDataAtual = new SimpleDateFormat("MMM d, yyyy");
        }
        Date testeData = new Date();
        try {
            testeData = formatoDataAtual.parse(dataN);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatarData = new SimpleDateFormat("yyyy/MM/dd");
        String novoFormato = formatarData.format(testeData);
        data = novoFormato;
        Log.i("data",data);
        imageView = (ImageView)findViewById(R.id.imgOuvidoria);
        edNome = (EditText) findViewById(R.id.edNome);
        edEmail = (EditText) findViewById(R.id.edEmail);
        edEndereco = (EditText) findViewById(R.id.edEndereco);
        edNumero = (EditText) findViewById(R.id.edNumero);
        edBairro = (EditText) findViewById(R.id.edBairro);
        edMensagem = (EditText) findViewById(R.id.edMensagem);
        btMenu = (Button) findViewById(R.id.btMenu);
        btMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), PrincipalActivity.class);
                startActivity(intent);
            }
        });
        btEnviar = (Button) findViewById(R.id.btEnviar);
        btEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Verificar();
            }
        });

        tvAnexo = (TextView)findViewById(R.id.tvAnexo);
        btAnexo = (Button)findViewById(R.id.btAnexo);
        btAnexo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.LOLLIPOP) {
                    // Do something for lollipop and above versions
                    if (getBaseContext().checkSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {

                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_REQUEST_CODE);
                    }
                    if (getBaseContext().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                MY_REQUEST_CODE);
                    }
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(OuvidoriaActivity.this);

                builder.setTitle("Abrir");

                builder.setPositiveButton("Galeria", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        LocalizarImagem();
                    }
                });

                builder.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        AbrirCamera();
                    }
                });

                alertaDialog = builder.create();
                alertaDialog.show();
            }
        });
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public void LocalizarImagem(){
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);

        intent.addCategory(Intent.CATEGORY_OPENABLE);

        intent.setType("image/*");

        startActivityForResult(intent, 42);
    }
    public void AbrirCamera(){

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {

            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.projampa.dsouza.projampa3.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");

        if (requestCode == MY_REQUEST_CODE) {
            if (grantResults.length <= 0) {

                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        localImagem = image.getAbsolutePath();
        return image;
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        imageView.setImageBitmap(image);
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 42 && resultCode == Activity.RESULT_OK) {

            if (data != null) {
                uri = data.getData();
                File file = new File(uri.getPath());
                localImagem = file.getAbsolutePath();
                tvAnexo.setText(localImagem);
                try {
                    getBitmapFromUri(uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Log.i(TAG, "Uri: " + uri.toString());
            }
        }

        if (requestCode == 1 & resultCode == RESULT_OK) {

                imageView.setImageBitmap(BitmapFactory.decodeFile(localImagem));
                tvAnexo.setText(localImagem);
        }
    }
    void Verificar() {

        if ( edNome.getText().toString().equals("")
                || edEndereco.getText().toString().equals("") || edMensagem.getText().toString().equals("")
                || edBairro.getText().toString().equals("")|| edEmail.getText().toString().equals("")|| edNumero.getText().toString().equals("") || tvAnexo.getText().equals("")) {
            Toast.makeText(getBaseContext(), "Por favor, preencha todas as colunas", Toast.LENGTH_SHORT).show();
        } else {
            EnviarImagem();
        }
    }

    void EnviarImagem(){
        mProgress.setMessage("Carregando Imagem");
        mProgress.show();
        Uri filePath = Uri.fromFile(new File(localImagem));
        if (filePath != null) {
            List<Integer> numeros = new ArrayList<Integer>();
            for (int i = 1; i < 61; i++) {
                numeros.add(i);
            }
            Collections.shuffle(numeros);
            MessageDigest mdEnc = null;
            try {
                mdEnc = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            mdEnc.update(numeros.toString().getBytes(), 0, numeros.toString().length());
            String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
            while (md5.length() < 32) {
                md5 = "0" + md5;
            }
            StorageReference childRef = storageRef.child("imagesAndroid/"+md5+".jpg");
            urlImagem = md5 + ".jpg";
            imageView.setDrawingCacheEnabled(true);
            //imageView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            //imageView.layout(0, 0, imageView.getMeasuredWidth(), imageView.getMeasuredHeight());
            imageView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            imageView.buildDrawingCache();
            Bitmap bitmap = Bitmap.createBitmap(imageView.getDrawingCache());
            Bitmap bit;
            bit = bitmap.createScaledBitmap(bitmap, 400, 400, false);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bit.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            byte[] data = outputStream.toByteArray();

            UploadTask uploadTask = childRef.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                mProgress.dismiss();
                @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                urlImagem = downloadUrl.toString();
                Log.i("url",urlImagem);
                Toast.makeText(OuvidoriaActivity.this, "Imagem Carregada", Toast.LENGTH_SHORT).show();
                Enviar();
            }}).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(OuvidoriaActivity.this, "Falha ao Carregar Imagem", Toast.LENGTH_SHORT).show();
                }
        });

        }
    }

    void Enviar() {

        final String nomeString = edNome.getText().toString().trim();
        final String emailString = edEmail.getText().toString().trim();
        final String enderecoString = edEndereco.getText().toString().trim();
        final String numeroString = edNumero.getText().toString().trim();
        final String bairroString = edBairro.getText().toString().trim();
        final String mensagemString = edMensagem.getText().toString().trim();

        class Enviar extends AsyncTask<Void, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {

                super.onPreExecute();

                loading = ProgressDialog.show(OuvidoriaActivity.this, "Enviando sua mensagem", "Por favor, aguarde", false, false);
            }

            @Override
            protected void onPostExecute(String s) {

                super.onPostExecute(s);

                loading.dismiss();

                    edNome.setText("");
                    edBairro.setText("");
                    edNumero.setText("");
                    edEndereco.setText("");
                    edEmail.setText("");
                    edMensagem.setText("");
                    Toast.makeText(OuvidoriaActivity.this, "Sua mensagem foi enviada com sucesso!!", Toast.LENGTH_LONG).show();

            }

            @Override
            protected String doInBackground(Void... v) {

                HashMap<String, String> parametros = new HashMap<>();
                parametros.put(Config.KEY_EMP_NOME, nomeString);
                parametros.put(Config.KEY_EMP_EMAIL, emailString);
                parametros.put(Config.KEY_EMP_IMAGEM, urlImagem);
                parametros.put(Config.KEY_EMP_RUA, enderecoString);
                parametros.put(Config.KEY_EMP_NUMERO, numeroString);
                parametros.put(Config.KEY_EMP_BAIRRO, bairroString);
                parametros.put(Config.KEY_EMP_MENSAGEM, mensagemString);
                parametros.put(Config.KEY_EMP_DATA, data);

                RequestHandler rh = new RequestHandler();


                String res = rh.sendPostRequest(Config.URL_OUVIDORIA, parametros);

                return res;
            }
        }
        Enviar enviar = new Enviar();

        enviar.execute();
    }
}

