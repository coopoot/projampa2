package com.projampa.dsouza.projampa3.Controller;

/**
 * Created by dsouza on 26/05/17.
 */

public class ListaTransparencia {
    String titulo;
    String data;
    String img;

    public ListaTransparencia(String titulo, String data, String img){
        this.titulo = titulo;
        this.data = data;
        this.img = img;
    }
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
