package com.projampa.dsouza.projampa3.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.projampa.dsouza.projampa3.R;

import java.util.UUID;

public class IntroActivity extends AppCompatActivity {

    long unique = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent i = new Intent(IntroActivity.this, PrincipalActivity.class);
                startActivity(i);
                finish();
            }
        }, 2000);

        SharedPreferences sharedPreferences = getSharedPreferences("unique", 0);
        unique = sharedPreferences.getLong("idUnique2",unique);
        if(unique == 0){
            UUID myuuid = UUID.randomUUID();
            long bits1 = myuuid.getMostSignificantBits();
            long bits2 = myuuid.getLeastSignificantBits();
            long i = bits1+bits2;
            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.putLong("idUnique2", Math.abs(i));
            editor.commit();
        }else{
            Log.i("UNI", String.valueOf(unique));
        }

    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
}
