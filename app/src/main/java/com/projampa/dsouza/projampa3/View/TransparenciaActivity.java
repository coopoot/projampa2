package com.projampa.dsouza.projampa3.View;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.projampa.dsouza.projampa3.Controller.ListaTransparencia;
import com.projampa.dsouza.projampa3.Model.ListaAdapter;
import com.projampa.dsouza.projampa3.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class TransparenciaActivity extends Activity implements AdapterView.OnItemClickListener{

    ArrayList<String> arrayImg;
    ArrayList<String> arrayId;
    ArrayList<String> arrayData;
    ArrayList<String> arrayTitulo;
    ArrayList<String> arrayAssunto;
    ListView listView;
    ArrayList<ListaTransparencia> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected()) {
        }else{
            Toast.makeText(getBaseContext(), "Sua conexão com a internet falhou!!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,PrincipalActivity.class);
            startActivity(intent);
        }

        setContentView(R.layout.activity_transparencia);
        listView = (ListView)findViewById(R.id.listView);
        arrayImg = new ArrayList<>();
        arrayId = new ArrayList<>();
        arrayData = new ArrayList<>();
        arrayTitulo = new ArrayList<>();
        arrayAssunto = new ArrayList<>();
        arrayList = new ArrayList<>();
        listView.setOnItemClickListener(this);

        carregarDadosServidor();

    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    private void carregarDadosServidor() {

        @SuppressLint("StaticFieldLeak") AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(TransparenciaActivity.this, "Carregando ", "Por favor, aguarde", false, false);
            }

            @Override
            protected Void doInBackground(Integer... integers) {

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://projampathiago.hospedagemdesites.ws/wp-json/wp/v2/posts?categories=3")
                        .build();
                try {
                    Response response = client.newCall(request).execute();

                    JSONArray array = new JSONArray(response.body().string());


                for(int i = 0; i < array.length();i++) {
                    JSONObject object = array.getJSONObject(i);
                    arrayId.add(object.getString("id"));
                    //converter data
                    String data = object.getString("date");
                    SimpleDateFormat formatoDataAtual = new SimpleDateFormat("yyy-mm-dd'T'hh:MM:ss");
                    Date testeData = null;
                    try {
                        testeData = formatoDataAtual.parse(data);
                    }catch(Exception ex){
                        ex.printStackTrace();
                    }
                    SimpleDateFormat formatarData = new SimpleDateFormat("mm/dd/yyyy hh:mm:ss a");
                    String novoFormato = formatarData.format(testeData);
                    arrayData.add(novoFormato);

                    String titulo = object.getString("title");
                    String content = object.getString("content");
                    String urlImg = object.getString("better_featured_image");
                    JSONObject assuntoObj = (JSONObject) new JSONTokener(content).nextValue();
                    arrayAssunto.add(assuntoObj.getString("rendered"));
                    JSONObject tituloObj = (JSONObject) new JSONTokener(titulo).nextValue();
                    arrayTitulo.add(tituloObj.getString("rendered"));
                    JSONObject imgObj = (JSONObject) new JSONTokener(urlImg).nextValue();
                    arrayImg.add(imgObj.getString("source_url"));
                    arrayList.add(new ListaTransparencia(
                            tituloObj.getString("rendered"),
                            novoFormato,
                            imgObj.getString("source_url")
                    ));
                    Log.i("LOG", arrayId.get(i));
                    Log.i("LOG", arrayData.get(i));
                    Log.i("LOG", arrayTitulo.get(i));
                    Log.i("LOG", arrayAssunto.get(i));
                    Log.i("LOG", arrayImg.get(i));

                }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                loading.dismiss();
                ListaAdapter adapter = new ListaAdapter(
                        getApplicationContext(), R.layout.list_transparencia, arrayList
                );
                listView.setAdapter(adapter);
            }
        };

        task.execute();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        EnviarDados(position);
    }
    void EnviarDados(int position){
        Intent intent = new Intent (this,CarregarTransparenciaActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("data", arrayData.get(position));
        bundle.putString("titulo", arrayTitulo.get(position));
        bundle.putString("assunto", arrayAssunto.get(position));
        bundle.putString("img", arrayImg.get(position));
        intent.putExtras(bundle);
        startActivity(intent);
    }

}
