package com.projampa.dsouza.projampa3.View;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.projampa.dsouza.projampa3.Model.Config;
import com.projampa.dsouza.projampa3.Model.RequestHandler;
import com.projampa.dsouza.projampa3.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class ColaboreActivity extends Activity implements AdapterView.OnItemSelectedListener {

    EditText edNome;
    EditText edTelefone;
    EditText edEmail;
    EditText edMensagem;
    Button btMenu;
    Button btEnviar;
    String assunto;
    String[] assuntoArray = new String []{"Saúde","Educação","Mobilidade Urbana","Esporte",
            "Empreendedorismo","Turismo","Emprego e Renda","Infraestrutura","Meio Ambiente","Outros"};
    String projeto;
    String data;
    SimpleDateFormat formatoDataAtual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colabore);
        String dataN = DateFormat.getDateInstance().format(new Date());

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected()) {
        }else{
            Toast.makeText(getBaseContext(), "Sua conexão com a internet falhou!!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,PrincipalActivity.class);
            startActivity(intent);
        }

        if( Locale.getDefault().getCountry().equals("BR")) {
            formatoDataAtual = new SimpleDateFormat("dd/MM/yyyy");
        }else{
            formatoDataAtual = new SimpleDateFormat("MMM d, yyyy");
        }
        Date testeData = new Date();
        try {
            testeData = formatoDataAtual.parse(dataN);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatarData = new SimpleDateFormat("yyyy/MM/dd");
        String novoFormato = formatarData.format(testeData);
        data = novoFormato;
        Log.i("data",data);
        edNome = (EditText) findViewById(R.id.edNomeColabore);
        edTelefone = (EditText) findViewById(R.id.edTelefone);
        edEmail = (EditText) findViewById(R.id.edEmail);
        edMensagem = (EditText) findViewById(R.id.edMensagemColabore);
        btMenu = (Button) findViewById(R.id.btMenu);
        btMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(),PrincipalActivity.class);
                startActivity(intent);
            }
        });
        btEnviar = (Button) findViewById(R.id.btEnviar);
        btEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    EmailVerificar();
            }
        });
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,assuntoArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    public void VerificarRadio(){
        RadioGroup rdTipo = (RadioGroup) findViewById(R.id.rdTipo);
        switch (rdTipo.getCheckedRadioButtonId()){
            case R.id.rdProposta:
                projeto = "Proposta";
                Log.i("Tipo",projeto);
                break;
            case R.id.rdIdeia:
                projeto = "Ideia";
                Log.i("Tipo",projeto);
                break;
        }
        if(assunto == null || projeto == null || edNome.getText().toString().equals("")
                || edEmail.getText().toString().equals("")|| edMensagem.getText().toString().equals("")
                || edTelefone.getText().toString().equals("")){
            Toast.makeText(getBaseContext(), "Por favor, preencha todas as colunas", Toast.LENGTH_SHORT).show();
        }else{
        Enviar();
            }
    }

    void Enviar() {


        final String nomeString = edNome.getText().toString().trim();
        final String telefoneString = edTelefone.getText().toString().trim();
        final String emailString = edEmail.getText().toString().trim();
        final String mensagemString = edMensagem.getText().toString().trim();

        class Enviar extends AsyncTask<Void, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {

                super.onPreExecute();

                loading = ProgressDialog.show(ColaboreActivity.this, "Enviando sua colaboração", "Por favor, aguarde", false, false);
            }

            @Override
            protected void onPostExecute(String s) {

                super.onPostExecute(s);

                loading.dismiss();

                //if (s.equals("deu bom")) {

                    edNome.setText("");
                    edTelefone.setText("");
                    edEmail.setText("");
                    edMensagem.setText("");
                    Toast.makeText(ColaboreActivity.this,"Sua mensagem foi enviada com sucesso!!",Toast.LENGTH_LONG).show();


//                } else {
//
//                    Toast.makeText(ColaboreActivity.this,"Erro ao enviar sua mensagem, por favor tente mais tarde",Toast.LENGTH_LONG).show();
//                }

            }


            @Override
            protected String doInBackground(Void... v) {

                HashMap<String, String> parametros = new HashMap<>();
                parametros.put(Config.KEY_EMP_NOME, nomeString);
                parametros.put(Config.KEY_EMP_TELEFONE, telefoneString);
                parametros.put(Config.KEY_EMP_DATA, data);
                parametros.put(Config.KEY_EMP_EMAIL, emailString);
                parametros.put(Config.KEY_EMP_ASSUNTO, assunto);
                parametros.put(Config.KEY_EMP_CONTEUDO, mensagemString);
                parametros.put(Config.KEY_EMP_PROJETO, projeto);

                RequestHandler rh = new RequestHandler();

                String res = rh.sendPostRequest(Config.URL_COLABORE, parametros);

                return res;
            }
        }
        Enviar enviar = new Enviar();

        enviar.execute();
    }

    void EmailVerificar() {

        String getText = edEmail.getText().toString();

        String VrEm =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        if (getText.matches(VrEm) && getText.length() > 0) {

            VerificarRadio();

        } else {
            Toast.makeText(this,"Por favor digite um email válido!!",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        parent.getItemAtPosition(position);
        assunto = assuntoArray[position];
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
