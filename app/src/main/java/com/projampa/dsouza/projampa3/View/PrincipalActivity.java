package com.projampa.dsouza.projampa3.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.projampa.dsouza.projampa3.R;


public class PrincipalActivity extends AppCompatActivity {

    Button btnOuvidoria;
    Button btnColabore;
    Button btnTransparencia;
    Button btnVereador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        btnOuvidoria = (Button)findViewById(R.id.btnOuvidoria);
        btnOuvidoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), OuvidoriaActivity.class);
                startActivity(intent);
            }
        });
        btnColabore = (Button)findViewById(R.id.btnColabore);
        btnColabore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ColaboreActivity.class);
                startActivity(intent);
            }
        });
        btnTransparencia = (Button)findViewById(R.id.btnTransparencia);
        btnTransparencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), TransparenciaActivity.class);
                startActivity(intent);
            }
        });
        btnVereador = (Button)findViewById(R.id.btnVereador);
        btnVereador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), VereadorActivity.class);
                startActivity(intent);
            }
        });
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
}
