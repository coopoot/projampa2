package com.projampa.dsouza.projampa3.View;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.projampa.dsouza.projampa3.R;

public class CarregarTransparenciaActivity extends Activity {

    String titulo = "";
    String assunto = "";
    String html = "<html><body>";
    String img = "";
    String data = "";
    WebView webView;
    TextView tvData;
    TextView tvTitulo;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carregar_transparencia);
        Bundle bundle = getIntent().getExtras();

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected()) {
        }else{
            Toast.makeText(getBaseContext(), "Sua conexão com a internet falhou!!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,PrincipalActivity.class);
            startActivity(intent);
        }

        tvData = (TextView)findViewById(R.id.tvData);
        tvTitulo = (TextView)findViewById(R.id.tvTitulo);

        imageView = (ImageView)findViewById(R.id.img2);

        titulo = bundle.getString("titulo");
        assunto = bundle.getString("assunto");
        data = bundle.getString("data");
        img = bundle.getString("img");

        if(titulo != "" || data != "" || assunto != "" || img != ""){

            Glide.with(this)
                    .load(img)
                    .override(600, 300)
                    .centerCrop()
                    .into(imageView);

            tvData.setText(data);
            tvTitulo.setText(titulo);

            webView = (WebView)findViewById(R.id.webView);
            WebSettings ws = webView.getSettings();
            ws.setDefaultTextEncodingName("utf-8");
            ws.setJavaScriptEnabled(true);
            ws.setSupportZoom(false);

            html += assunto;
            html += "</body></html>";
            webView.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);
        }

    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
}
