package com.projampa.dsouza.projampa3.Model;

/**
 * Created by dsouza on 08/05/17.
 */

public class Config {
    //endereço do webservice
    public static final String URL_COLABORE = "http://projampathiago.tecnologia.ws/projampa/colabore.php";
    public static final String URL_INSERT = "http://projampathiago.tecnologia.ws/projampa/insert.php";
    public static final String URL_OUVIDORIA = "http://projampathiago.tecnologia.ws/projampa/ouvidoria.php";


    public static final String KEY_EMP_NOME = "nome";
    public static final String KEY_EMP_TELEFONE = "telefone";
    public static final String KEY_EMP_DATA = "data";
    public static final String KEY_EMP_EMAIL = "email";
    public static final String KEY_EMP_ASSUNTO = "assunto";
    public static final String KEY_EMP_CONTEUDO = "conteudo";
    public static final String KEY_EMP_PROJETO = "projeto";
    public static final String KEY_EMP_KEY = "key";

    public static final String KEY_EMP_ID = "id";
    public static final String KEY_EMP_TITULO = "titulo";
    public static final String KEY_EMP_VOTACAO = "votacao";

    public static final String KEY_EMP_RUA = "rua";
    public static final String KEY_EMP_BAIRRO = "bairro";
    public static final String KEY_EMP_NUMERO = "numero";
    public static final String KEY_EMP_MENSAGEM = "mensagem";
    public static final String KEY_EMP_IMAGEM = "imagem";


}
