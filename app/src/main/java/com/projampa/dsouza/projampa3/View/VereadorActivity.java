package com.projampa.dsouza.projampa3.View;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.projampa.dsouza.projampa3.Model.Config;
import com.projampa.dsouza.projampa3.Model.RequestHandler;
import com.projampa.dsouza.projampa3.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class VereadorActivity extends Activity {
    String content = "";
    String idProposta = "";
    String titulo= "";
    String data;
    String html = "<html>";
    int escolha = 0;
    Button btEnviar;
    Button btMenu;
    WebView webView;
    SimpleDateFormat formatoDataAtual;
    SharedPreferences sharedPreferences = null;
    String idVotacao = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vereador);

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isAvailable()
                && connectivityManager.getActiveNetworkInfo().isConnected()) {
        }else{
            Toast.makeText(getBaseContext(), "Sua conexão com a internet falhou!!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this,PrincipalActivity.class);
            startActivity(intent);
        }

        //VERIFICAR VOTAÇÃO
        sharedPreferences = getSharedPreferences("dados",0);
        idVotacao = sharedPreferences.getString("idVotacao",idVotacao);
        // FINAL
        //PEGAR DATA DO SISTEMA E CONVERTER PARA ENVIAR AO BANCO DE DADOS
        String dataN = DateFormat.getDateInstance().format(new Date());
        if( Locale.getDefault().getCountry().equals("BR")) {
            formatoDataAtual = new SimpleDateFormat("dd/MM/yyyy");
        }else{
            formatoDataAtual = new SimpleDateFormat("MMM d, yyyy");
        }
        Date testeData = new Date();
        try {
            testeData = formatoDataAtual.parse(dataN);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatarData = new SimpleDateFormat("yyyy/MM/dd");
        String novoFormato = formatarData.format(testeData);
        data = novoFormato;
        Log.i("data",data);
        // FINAL DATA
        //INICIO WEBVIEW DECLARAÇÃO E BOTÕES DE ENVIO, MENU
        webView = (WebView)findViewById(R.id.webView);
        btEnviar = (Button)findViewById(R.id.btEnviar);
        btEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(idVotacao.equals(idProposta)){
                    Toast.makeText(VereadorActivity.this, "Você já votou não pode votar novamente", Toast.LENGTH_SHORT).show();
                }else{
                    VerificarRadio();
                }
            }
        });
        btMenu = (Button)findViewById(R.id.btMenu);
        btMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(),PrincipalActivity.class);
                startActivity(intent);
            }
        });

        WebSettings ws = webView.getSettings();
        ws.setDefaultTextEncodingName("utf-8");
        ws.setJavaScriptEnabled(true);
        ws.setSupportZoom(false);
        //FINAL WEBVIEW E BOTÕES
        //INICIO DE CARREGAMENTO DE DADOS
        carregarDadosServidor(1);
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
    //METODO PARA VERIFICAR RADIO
    public void VerificarRadio(){
        RadioGroup rdTipo = (RadioGroup) findViewById(R.id.rdVotacao);

        switch (rdTipo.getCheckedRadioButtonId()){
            case R.id.rdEscolha1:
                escolha = 1;
                Log.i("escolha", String.valueOf(escolha));
                break;
            case R.id.rdEscolha2:
                escolha = 2;
                Log.i("escolha", String.valueOf(escolha));
                break;
        }
        if(escolha == 0 ){
            Toast.makeText(getBaseContext(), "Por favor, esolha uma das opções para votar", Toast.LENGTH_SHORT).show();
        }else{
            Enviar();
        }
    }
    //METODO DE ENVIO DE DADOS
    void Enviar() {

        final String escolhaString = String.valueOf(escolha);

        class Enviar extends AsyncTask<Void, Void, String> {

            ProgressDialog loading;

            @Override
            protected void onPreExecute() {

                super.onPreExecute();

                loading = ProgressDialog.show(VereadorActivity.this, "Enviando sua escolha", "Por favor, aguarde", false, false);
            }

            @Override
            protected void onPostExecute(String s) {

                super.onPostExecute(s);

                loading.dismiss();

               // if (s.equals("deu bom")) {

                    SharedPreferences.Editor editor =  sharedPreferences.edit();
                    editor.putString("idVotacao",idProposta);
                    editor.commit();
                    Toast.makeText(VereadorActivity.this,"Escolha enviada com sucesso!!",Toast.LENGTH_LONG).show();
                    sharedPreferences = getSharedPreferences("dados",0);
                    idVotacao = sharedPreferences.getString("idVotacao",idVotacao);

//                } else {
//
//                    Toast.makeText(VereadorActivity.this,"Erro ao enviar sua escolha, por favor tente mais tarde",Toast.LENGTH_LONG).show();
//                }

            }


            @Override
            protected String doInBackground(Void... v) {

                HashMap<String, String> parametros = new HashMap<>();
                parametros.put(Config.KEY_EMP_ID, idProposta);
                parametros.put(Config.KEY_EMP_TITULO, titulo);
                parametros.put(Config.KEY_EMP_DATA, data);
                parametros.put(Config.KEY_EMP_VOTACAO, escolhaString);

                RequestHandler rh = new RequestHandler();

                String res = rh.sendPostRequest(Config.URL_INSERT, parametros);

                return res;
            }
        }
        Enviar enviar = new Enviar();

        enviar.execute();
    }
    //CONTRUIR O HTML APOS A CONSULTA
    public void Html(String query){
        html += "<body>";
        html += query;
        html += "</body></html>";
        webView.post(new Runnable() {
            @Override
            public void run() {
                webView.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);
            }
        });

    }
    //CARREGAMENTO DE DADOS
    private void carregarDadosServidor(int id) {

        AsyncTask<Integer, Void, Void> task = new AsyncTask<Integer, Void, Void>() {
            ProgressDialog loading;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(VereadorActivity.this, "Carregando ", "Por favor, aguarde", false, false);
            }

            @Override
            protected Void doInBackground(Integer... integers) {

                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://projampathiago.hospedagemdesites.ws/wp-json/wp/v2/posts?categories=2")
                        .build();
                try {
                    Response response = client.newCall(request).execute();

                    JSONArray array = new JSONArray(response.body().string());

                    if(array.length()!= 0) {

                            JSONObject object = array.getJSONObject(0);
                            idProposta = object.getString("id");
                            String title = object.getString("title");
                            JSONObject tituloObj = (JSONObject) new JSONTokener(title).nextValue();
                            titulo = tituloObj.getString("rendered");
                            content = object.getString("content");

                    }
                    JSONObject object = (JSONObject) new JSONTokener(content).nextValue();
                    String query = object.getString("rendered");
                    Log.i("LOG", idProposta);
                    Log.i("LOG",titulo);
                    Log.i("LOG",query);
                    Html(query);

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    System.out.println("End of content");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                loading.dismiss();
            }
        };

        task.execute(id);
    }
}

