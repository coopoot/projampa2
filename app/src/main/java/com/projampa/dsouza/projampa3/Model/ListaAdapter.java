package com.projampa.dsouza.projampa3.Model;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.projampa.dsouza.projampa3.Controller.ListaTransparencia;
import com.projampa.dsouza.projampa3.R;

import java.util.ArrayList;

/**
 * Created by dsouza on 26/05/17.
 */

public class ListaAdapter extends ArrayAdapter<ListaTransparencia> {

    ArrayList<ListaTransparencia> listaTransparencias;
    Context context;
    int id;

    public ListaAdapter(Context context, int id, ArrayList<ListaTransparencia> listaTransparencias) {
        super(context, id, listaTransparencias);
        this.listaTransparencias = listaTransparencias;
        this.context = context;
        this.id = id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext()
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_transparencia, null, true);

        }
        ListaTransparencia listaTransparencia = getItem(position);

        ImageView imageView = (ImageView) convertView.findViewById(R.id.img2);
        Glide.with(context)
                .load(listaTransparencia.getImg())
                .override(600, 300)
                .centerCrop()
                .into(imageView);

        TextView txtTitulo = (TextView) convertView.findViewById(R.id.tvTitulo);
        txtTitulo.setText(listaTransparencia.getTitulo());

        TextView txtData = (TextView) convertView.findViewById(R.id.tvData);
        txtData.setText(listaTransparencia.getData());

        return convertView;
    }
}